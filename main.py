import sys
import socket
import argparse
import time
from lan_detect import LanDetect

def detect_tcp(ip, **ports):
    port_info = []
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(1)
    for port in range(ports['start_port'], ports['end_port']):
        result = s.connect_ex((ip, port))
        print(result)
        if (result == 0):
            try:
                serv = socket.getservbyport(port, 'tcp')
            except Exception as identifier:
                serv = "unknow"
            finally:
                pass
            port_info.append({port: serv})
    s.close()
    return port_info

if __name__ == '__main__':
    start = time.time()
    lan = LanDetect()
    lan_ips = lan.mix_host()
    
    ip_list_info = {}
    for ip in lan_ips:
        if lan.get_ping(ip):
            res = detect_tcp(ip, start_port = 15, end_port = 100)
            print(res)
            if bool(res):
                ip_list_info[ip] = res

    print(ip_list_info)
    end = time.time()
    print("Total spend time seconds: %s" % (end - start))

    # parser = argparse.ArgumentParser(usage = "check socket statistics")
    # parser.add_argument('-v', help='show detail info')
    # args = parser.parse_args()