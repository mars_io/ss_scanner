import logging

class MakeMd:

    md_file_handle = None
    md_file_text = ""

    def __init__(self):
        try:
            self.md_file_handle = open("./report.md", 'w+')        
        except Exception as identifier:
            logging.debug('file io error')

    def __del__(self):
        self.md_file_handle.close()

    def set_report_title(self, text = "# Lan Host Info \n\n"):
        self.md_file_text += text

    def set_report_desc(self, text = "Hi, Welcome to this report! It reported your check result list. \n\n"):
        self.md_file_text += text

    def set_ip_title(self, ip):
        self.md_file_text += "```IP address %s```\n\n" % (ip)

    def set_port_table(self, items = "None active ports"):
        self.md_file_text += "Port | Server\n"
        self.md_file_text += ":-: | :-:\n"
        self.md_file_text += items

    def out_md(self):
        self.set_report_title()
        self.set_report_desc()
        self.set_ip_title('192.11.1.11')
        self.set_port_table()
        self.md_file_handle.writelines(self.md_file_text)


if __name__ == "__main__":
    md = MakeMd()
    md.out_md()