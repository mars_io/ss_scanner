import platform
import sys
import socket
import os

class LanDetect:

    def __init__(self):
        pass

    def __del__(self):
        pass
    '''
    Get ping count params,
    with windows or linux
    '''
    def get_os(self):
        return 'n' if platform.system() == "Window" else 'c'

    '''Check host wheather active with ping tools'''
    def get_ping(self, ip):
        cmd = ['ping', '-{} {} -{} {}'.format(self.get_os(),'2', 'W', '5'), ip]
        output = os.popen(" ".join(cmd)).readlines()
        check_active = lambda s: "".join(s).upper().find('TTL') > 0
        return check_active(output)

    '''Enumerate all ip address in vlan'''
    def mix_host(self, ip = None):
        if bool(ip):
            ip_parts = ip.split('.')
            ip_parts.pop()
            ip_list = []
            for i in range(1, 256):
                ip_parts.append(str(i))
                ip_list.append('.'.join(ip_parts))
                ip_parts.pop()
            return ip_list
        else:
            return ['127.0.0.1']

    '''Get local ip address'''
    def get_local(self):
        socket_handle = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        socket_handle.connect(('8.8.8.8', 80))
        ip_addr = socket_handle.getsockname()[0]
        socket_handle.close()
        return ip_addr

if __name__ == '__main__':
    lan = LanDetect()
    lan_ips = lan.mix_host('192.168.1.1')
    for ip in lan_ips:
        if lan.get_ping(ip):
            print("Active IP: " + ip )


